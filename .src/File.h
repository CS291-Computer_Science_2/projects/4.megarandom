#ifndef FILE_H
#define FILE_H
///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: Provide the data type to store the file data and other file info
//////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
using namespace std;

struct File {
	string fileName;
	char delimiter;
	int nrRows;
	vector<vector<string> > vFile; //holds actual file data
	
	File() {}
	File(string fileNme, char del, int nRows, vector<vector<string> > &vF) : fileName(fileNme)
		,delimiter(move(del)), nrRows(move(nRows)), vFile((move(vF))) {}

};

#endif
