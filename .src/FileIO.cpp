///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: read and write to files
// Pseudo Code: (1) Ask user for the file names
//				(2) Check if input files exist
//				(3) Allow the user to change the order of files to feed the randomizer
//				(4) Allow the user to read only from certain files
//					(the user might want to generate multiple files with the same data)
//				(5) Allow the user to strip the file header in each file (junk data)
//				(6) Read all the files in the following way:
//					(6.1) Read a file to a string vector of vectors (can handle many columns)
//					(6.2) Place the string vec of vec's into a vector of 'File' data type
//						(6.2.1) Additional data in 'File' vector includes:
//								  . file name
//								  . number of rows in each file
//								  . column delimiter
//					(6.3) Return a 'File' type vector for use in the randomizer module
//
//////////////////////////////////////////////////////////////////
#include "FileIO.h"


FileIO::FileIO() {
	/* TODO (#4#): MOVE 'tool' OBJECT INITIALIZATION TO HERE */
}

FileIO::~FileIO() {
	/* TODO (#4#): DELETE 'tool' OBJECT ONCE IMPLEMENTED INTO THE CONSTRUCTOR*/
}

void FileIO::setFileNames(vector<string> &vFNames, string &outfile) {
	string tmp;
	int fnr;

	cin.get();
	cout<<">>Enter the name for the output file: ";
	getline(cin, tmp);

	cout<<"\n>>How many files will you draw data from? ";
	cin>>fnr;
	cin.get();
	cout<<">>>>enter their names(including extention): \n";

	vector<string> vtmp;
	for(int a=0; a<fnr; ++a) {
		printf("\t (%d): ",a+1);
		getline(cin, tmp);
		vtmp.push_back(tmp);
	}

	vFNames = vtmp;
//#TEST: FILE NAMES STORED PROPERLY#
//	cout<<"\nvtmp size:"<<vTmp.size();
//	for(unsigned int a=0; a<vTmp.size(); ++a) {
//		if(a==0) {
//			cout<<"\n  Output file: "<<vTmp.at(0)
//			    <<"\n  Input files:";
//		} else
//			cout<<"\n  "<<a<<": "<<vTmp.at(a);
//	}
}

void FileIO::reorderFileNames(vector<string> &vFN) {
	cout<<"\n Select the files in the order to be read.\n";
	vector<string> vOut;

	for(unsigned int a= 0; a<vFN.size(); ++a) {
		unsigned int choice= 0, counter= 1;
		for(auto &it: vFN) { //display menu with filenames
			printf("\t(%d) %s\n",counter,it.c_str());
			++counter;
		}

		do { //user selects an option from the menu above
			cout<<">>You choose: ";
			cin>>choice;
		} while (choice<1 || choice>vFN.size());

		vOut.emplace_back(vFN.at(choice-1));

		vFN.erase(vFN.begin()+(choice-1)); //remove already selected files
	}
	vFN= vOut; //reorder file names
}

vector<File> FileIO::filesReader(const vector<string> &vFileNames, int &nrFiles) {
	bool error= false;
	int ch= 0;
	vector<string> vfNames;

	cout<<"\n  Which files would you like to open?"
	    <<"\n\t\t(1) All"
	    <<"\n\t\t(2) Select which ones";
	do  {
		cout<<"\n>>You Choose: ";
		ch= tool->cinNrInput();
	} while(ch<1 || ch>2);

	if(ch==1) vfNames= vFileNames; //All files will be read

	else if(ch==2) { //Set which files to read
		for(auto &it: vFileNames) {
			string yn= "";
			printf("\tRead the data in '%s'  ?",it.c_str());
			cin>>yn;

			if(yn.at(0)=='y' || yn.at(0)=='y') //Check the character at string index 1
				vfNames.emplace_back(it);
		}
	} else cout<<"\n\t/!\\ERROR: Broken menu in FileIO::filesReader /!\\ \n";

	char delimiter= ' ';
	int nrRows= 0;
	vector<File> vObjFiles; //Will hold all the files

	/*Read each file to a vector of 'File' data type*/
	for (unsigned int ctr=0; ctr<vfNames.size(); ++ctr ) {
		vector<vector<string> > vvTemp;
		readFile(vfNames.at(ctr),delimiter,nrRows,vvTemp); //place file data into vvTemp

		File f;
		f.delimiter= delimiter;
		f.fileName= vfNames.at(ctr);
		f.nrRows= nrRows;
		f.vFile= vvTemp;

		vObjFiles.push_back(f);
		++nrFiles; //count the number of files
	}

	return vObjFiles;
//	//TEST: PRINT VECTOR OF VECTORS
//	tool->printVecOfVecs(vObjFiles[0].vFile);
//	tool->printVecOfVecs(vObjFiles[1].vFile);
}


void FileIO::readFile(const string &fileName, char &delimiter, int &nrRows,
                      vector<vector<string> > &vFileData) {
	bool stripHdr;
	string yn, multCols;

	ifstream ifs(fileName.c_str());

	printf("\n  Does '%s' have multiple columns?",fileName.c_str());
	cin>>multCols;
	if(multCols.at(0)=='y' || multCols.at(0)=='Y') {
		cin.get();
		printf("\n  How are columns separated in '%s' ?",fileName.c_str());

		/* TODO (#5#): NOT WORKING FOR SPACE DELIMITER */
		/* TODO (#5#): IMPLEMENT MULTI-CHAR DELIMITER */
		//Allow the user to enter space DELIMITER.
		string del;
		getline(cin,del);
		delimiter= del.at(0);
	}

	printf("\n  Do you want to strip the first row(headers)?");
	cin>>yn;
	if(yn.at(0)=='y' || yn.at(0)=='Y') stripHdr= true;

	if(!ifs.good()) {
		printf("\n\t/!\\ERROR: failed to open %s /!\\\n",fileName.c_str());
		return;
	}

	string rowData;
	while(getline(ifs,rowData,'\n')) { //read full row
		vector<string> vTemp;
		tool->splitString(rowData,delimiter,vTemp); //split columns
		vFileData.push_back(move(vTemp));
		++nrRows; //row counter
	}
}

void FileIO::saveFile(const vector<vector<string> > &vvRandData, string file) {
	ofstream ofs(file.c_str());

	for(auto row: vvRandData) { //row
		for(auto col: row) { //column
			ofs << col;
		}
		ofs<<endl;
	}
	ofs.close();
}





