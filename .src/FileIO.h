#ifndef FILEIO_H
#define FILEIO_H
///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: handle fire read/write operations
//////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
#include <fstream>
#include "File.h"
#include "Tools.h"
using namespace std;

/* TODO (#7#): FIGURE OUT HOW TO PROPERLY IMPLEMENT FORWARD DECLARATION VS HEADER INCLUDE */
//class Tools;

class FileIO {
	public:
		FileIO();
		~FileIO();

		void setFileNames(vector<string>&, string&); //user inputs file names
		void reorderFileNames(vector<string>&);
		vector<File> filesReader(const vector<string>&, int& nrFiles); //calls readFile per each file
		void saveFile(const vector<vector<string> > &vvRandData, string file);
	private:
		/*Speedy file reader*/
		void readFile(const string &fileName, char &delimiter, int &nrRows
		              ,vector<vector<string> > &vFileData);
		
		/* TODO (#4#): IMPLEMENT 'tool' ON CONSTRUCTOR. DELETE ON DESTRUCTOR */		
		Tools *tool;
};

#endif

