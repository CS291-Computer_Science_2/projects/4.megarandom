/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef MEGARANDOM_PRIVATE_H
#define MEGARANDOM_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"1.0.0.0"
#define VER_MAJOR	1
#define VER_MINOR	0
#define VER_RELEASE	0
#define VER_BUILD	0
#define COMPANY_NAME	"Jose Madureira LLC"
#define FILE_VERSION	"1.0.0.0"
#define FILE_DESCRIPTION	"Randomize existing data into a formatted output"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	""
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	""
#define PRODUCT_NAME	"Mega Random Generator"
#define PRODUCT_VERSION	"1.0.0.0"

#endif /*MEGARANDOM_PRIVATE_H*/
