///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: combine number randomizer with data access
//////////////////////////////////////////////////////////////////
#include "RandoManager.h"

RandoManager::RandoManager() {
	tool= new Tools;
}
RandoManager::~RandoManager() {
	delete tool;
}

/* TODO (#6#): BETTER EXPLANATION FOR EACH STEP */
vector<vector<string> > RandoManager::randyMan(const vector<File> &vFiles, int nrFiles) {
//	//TEST NUMBER OF FILES RECEIVED
//	cout<<"\n TEST: nrFiles:  "<<nrFiles<<endl;

	int maxRows;
	cout<<"\n  How many rows do you want to generate? \n";
	cin>>maxRows;

	/*Run once per file*/
	vector<vector<string> > vvFilesRands;
	for(int atFile= 0; atFile<nrFiles; ++atFile) {
		unsigned int atRow= 0;
		/* TODO (#7#): IMPLEMENT DYNAMIC COL SIZE AT EACH ROW FOR 'atRow' */
		unsigned int colCount= vFiles[atFile].vFile[atRow].size(); //vFile is vec of vec's with file data

		/*Generate random numbers between 0 and the number of rows in a file*/
		vector<int> vRands;
		vector<string> vColsData;
		for(unsigned int atCol= 0; atCol<colCount; ++atCol) { //per each column in a file
//			int nRows= vFiles[atFile].vFile.size();
			vRands= tool->randomizer(maxRows,0,vFiles[atFile].nrRows-1);

			/*Grab data from each column at all randomized positions*/
			vColsData= fetchRowsAtCol(vFiles[atFile].vFile,atCol,vRands);
		}

		/*Combine new random data from each file*/
		vvFilesRands.push_back(vColsData);
	}
	return vvFilesRands;
}

/*Grab data at specified rows, from a single column*/
vector <string> RandoManager::fetchRowsAtCol(const vector<vector<string> > &vvFileData
        ,const int col,const vector<int> &vRowsPos) {

	vector<string> vTemp;
	for(unsigned int ctr= 0; ctr<vRowsPos.size(); ++ctr) {
		int atRowX= vRowsPos.at(ctr); //vRowsPos contains the row numbers to fetch
		
		/* TODO (#1#): IMPLEMENT DYNAMIC COLUMN */		
		string st= vvFileData[atRowX][0]; //column remains constant
		vTemp.emplace_back(st); 
		cout<<endl<<st;
	}

	/*TEST, data at random rows*/
//	cout<<endl;
//	for(const auto &it: vTemp) {
//		cout<<"\t"<<it;
//	}
//	cout<<endl;

	return vTemp;
}


