#ifndef RANDOMANAGER_H
#define RANDOMANAGER_H
///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: combine number randomizer with data access
// Pseudo code:	(1) Get a range of randomized numbers
//				(2) Grab data from each column at each randomized index
//				(3) Return properly ordered vector to the data manager
//////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
#include "File.h"
#include "Tools.h"
using namespace std;

class RandoManager {
	public:
		RandoManager();
		~RandoManager();

		/*Organized Randomizer*/
		vector<vector<string> > randyMan(const vector<File> &vFiles, int nrFiles);
		/*Grab data at specific rows in a column*/
		vector <string> fetchRowsAtCol (const vector<vector<string> > &vvRC
		                                ,int col,const vector<int> &vRowsPos);
	private:
		Tools *tool;
};

#endif
