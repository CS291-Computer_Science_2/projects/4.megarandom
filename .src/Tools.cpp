///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: general purpose tools
// Pseudo Code:
//				(1) Comments throughout each individual implementation
//////////////////////////////////////////////////////////////////
#include "Tools.h"
//#include <vector>
//using namespace std;

int Tools::cinNrInput() {
	int nr;
	do {
		cin>>nr;
		if(cin.good())	break;
		cinFix();
	} while(!(cin.good()));
	return nr;
}

string Tools::rptChar(const char &rep, const int &n) {
	return string(n,rep);
}
string Tools::rptChar(const char &rep, const int &n, const char &end) {
	return string(n,rep)+string(1,end);
}

void Tools::cinFix() { //accept only numbers on input
	cin.clear();
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	cout << "\nNon-numeric characters found, try again: ";
	cin.get();
}

/*Randomize x number of integers between two bounds*/
vector<int> Tools::randomizer(int quantity, int low, int high) {
//	srand(time(0));
//	auto const seed = rand();

	/*Create a seed*/
	auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();

	/*Seed the randomizer and limit ranges*/
	mt19937 mt(seed);
	uniform_int_distribution<int> ran(low, high);

	vector<int> vNrs;
	for (int ctr=0; ctr<quantity; ++ctr) {
		vNrs.emplace_back(ran(mt));
		printf("\n\tRandom Number at vTemp pos %d is: %d",ctr,vNrs.at(ctr)); //REMOVE LATER
	}
	return vNrs;
}

/*Reorder columns in a single row*/
void Tools::colSwapper(vector<string> &vRow, const vector<int>& newOrder) {
	vector<string> vNewRow;
	for(unsigned int a=0; a<newOrder.size(); ++a) {
		int theColPos= newOrder.at(a); //get the col position to read
		vNewRow.emplace_back(vRow.at(theColPos)); //copy column data in the proper order
	}

	vRow= vNewRow;
}

/* TODO (#9#): SPACE DELIMITER NOT WORKING */
void Tools::splitString(const string& row, char delim, vector<string> &vSplit) {
	istringstream iss(row.c_str());

	string col="";
	vector<string> vTemp;
	while(getline(iss,col,delim)) {
		vSplit.emplace_back(col);
	}
}

void Tools::printVecOfVecs(vector<vector<string> >& vvRandomData, int nrRows) {
	int ctr=0 ;
	bool breaker= 0;
	for ( auto row: vvRandomData) {
		for(auto col: row) {
			cout<<col;
		}
		cout<< endl;

		//breaks after defined number of rows
		++ctr;
		if(ctr!=0 && ctr==nrRows) break;
	}
}


/*
###################################
###		UNUSED TOOLS SECTION	###
###	FUTURE PROCESS IMPROVEMENTS ###
###################################
*/

//REF: Sviatoslav  http://stackoverflow.com/a/37454181/6793751
/*NOT WORKING*/
vector<string> Tools::splitMultDels(const string& str, const string& delim) {
	vector<string> tokens;
	size_t prev = 0, pos = 0;
	do {
		pos = str.find(delim, prev);
		if (pos == string::npos)
			pos = str.length();

		string token = str.substr(prev, pos-prev);
		if (!token.empty())
			tokens.push_back(token);
		prev = pos + delim.length();
	} while (pos < str.length() && prev < str.length());
	return tokens;
}

