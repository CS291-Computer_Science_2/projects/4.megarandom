#ifndef TOOLS_H
#define TOOLS_H
///////////////////////////////////////////////////////////////////
// Name: Jose Madureira
// Date: 2016-12-13
// File purpose: general purpose tools for reuse
//////////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
#include <limits> //check for numbers or letters
#include <random> //mt19937 random
#include <chrono> //seed mt19937
//#include <ctime>
#include <sstream> //istringstream

using namespace std;

class Tools {
	public:
		Tools() {}
		~Tools() {}
				
		/*Only accept valid numbers as input*/
		int cinNrInput();
		/*Repeat a character n times*/
		string rptChar(const char &rep, const int &n);
		string rptChar(const char &rep, const int &n, const char &end); //add a char after the repetition ends
		vector<int> randomizer(int quantity, int low, int high); // mt19937 integer randomizer
		void colSwapper(vector<string> &vRow, const vector<int>& newOrder); //reorders columns
		
		void splitString(const string& row, char delim, vector<string> &vSplit);
		void printVecOfVecs(vector<vector<string> >& vvTemp, int nrRows= 0); //optional number of rows						
	private:		
		void cinFix(); //Fix broken cin. Called only by cinNrInput
		

		/*###########################################
		#UNUSED METHODS. FUTURE PROCESS IMPROVEMENTS#
		#############################################*/
				
		/*Handle multi-char delimiter*/
		vector<string> splitMultDels(const string& row, const string& delim); 
};
#endif
