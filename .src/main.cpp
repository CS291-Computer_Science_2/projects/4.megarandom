////////////////////////////////////////////////////////////////////////////////////////////
//  Name: Jose Madureira	CS291-Fall 2016
//	Date: 2016-12-13
//	Project: Final Project - Mega Random Generator
//	Version: 2.0
//			 Module status:
//				. setFileNames (no denoted issues)
//				. randomizer (no denoted issues)
//				. reorderFileNames (no denoted issues)
//				. filesReader (can't handle either multi-char delimiter, or space delimiter)
//				. Tools (all working except multi-char delimiter splitter)
//
//  Pseudo Code: (1) Open n number of user defined files
//				 (2) Combine the files into a single vector
//				 (3) Allow the user to select how the output data will be formatted
//				 (4) Randomly pick generated data from each file
//				 (5) Save randomized data to a file
//				 (6) Allow the user to generate more filesS
////////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include "Tools.h"
#include "FileIO.h"
#include "RandoManager.h"
//#include "DataManager.h"
using namespace std;

void setFileNames();
int main() {
	cout <<"\t..........................................\n"
	     <<"\t\t###Mega Random Generator###\n"
	     <<"\t              Jose Madureira\n"
	     <<"\t..........................................\n";

	int ch;

	Tools *tool= new Tools(); //useful tools
	FileIO *fio= new FileIO(); //file related operations
	RandoManager *ranMan= new RandoManager(); //Manage randomization
	

	string outFileName;
	int nrFiles;
	vector<string> inFileNames;
	vector<File> vAllFiles;
	vector<vector<string> > vvTheRandomizedData;

	/* TODO (#3#): ADD A WAY FOR USER TO PREMATURELY EXIT FROM EACH MENU OPTION
	   			    AND RETURN TO MAIN MENU */
	do {
		printf("\n\n [Main Menu] ");
		printf("\n    (1) Set File Names");
		printf("\n        (2) Change File Read Order");
		printf("\n    (3) Read Files");
		printf("\n    (4) Change Column Format"); //Not yet implemented
		printf("\n    (5) Randomize Data"); //Not Implemented yet
		printf("\n    (6) Pre-view Format/save fIle");
		printf("\n    (7) Quit\n");
		do  {
			cout<<"\n>>You Choose: ";
			ch= tool->cinNrInput();
		} while(ch<1 || ch>7);

		/*Make the user enter the file names before executing other operations*/
		if((ch!=1 && inFileNames.size()==0) && ch!=7) {
			cout<<"\n /!\\ Please set the file names first! /!\\"<<endl;
			fio->setFileNames(inFileNames, outFileName);
		}

		/*Execute Menu Option*/
		switch (ch) {
			case 1:
				fio->setFileNames(inFileNames, outFileName);
//TEST:		    cout<<"\n\ninFileNames size: "<<inFileNames.size()<<" , outFileName"<<outFileName<<endl;
				break;
			case 2:
				fio->reorderFileNames(inFileNames);
				break;
			case 3:
				vAllFiles= fio->filesReader(inFileNames, nrFiles); //Grab all the files and their info
				break;
			case 4:
				/* TODO (#1#): ALREADY DONE, JUST CALL IT HERE */								
				break;
			case 5:
				vvTheRandomizedData= ranMan->randyMan(vAllFiles, nrFiles); //get random data from files
				break;
			case 6:
				fio->saveFile(vvTheRandomizedData,outFileName);
				tool->printVecOfVecs(vvTheRandomizedData);
				break;
			case 7:
				delete ranMan;
				delete fio;
				delete tool;
				break;
			default:
				cout<<"\n\t\t/!\\Invalid Menu Selection/!\\\n";
		}
	} while(ch!=7);


	cout<<"\n\t\tGoodbye!\n\t";
	system("pause");
	return 0;
}





